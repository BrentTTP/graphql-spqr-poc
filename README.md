# GraphQL SQPR

> GraphQL SPQR (GraphQL Schema Publisher & Query Resolver, pronounced like speaker) is a simple-to-use library for rapid development of GraphQL APIs in Java.

## Pros

- Code first
- No Code duplicates e.g. schema & dto
- Builds on top of existing spring boot impl
- Does not require functional changes to existing code

## How to

GraphQL SPQR works with annotations. In a standard spring boot environment
with repos, services you can add `@GraphQLApi` to your service and `@GraphQLQuery`
or `@GraphQLMutation` to its methods. 

```java
@Service
@GraphQLApi
public class MyService {
    
    private MyRepo myRepo;
    
    @GraphQLQuery(name = "objects", description = "Retrieve all objects from repo")
    public List<MyObject> getObjects() {
        return myRepo.findAll();
    }
    
    @GraphQLMutation(name = "addObject")
    public MyObject addObject(@GraphQLArgument(name = "field1") String field1, 
                              @GraphQLArgument(name = "field2") int field2) {
        MyObject myObject = new MyObject(field1, field2);
        myRepo.save(myObject);
        return myObject;
    }
}
```

And that's it really, no schema file, no resolvers, just slap a few
annotations here and there and you have a working spring boot app with
GraphQL.