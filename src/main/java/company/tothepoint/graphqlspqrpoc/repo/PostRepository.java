package company.tothepoint.graphqlspqrpoc.repo;

import company.tothepoint.graphqlspqrpoc.model.Post;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends PagingAndSortingRepository<Post, ObjectId> {
    List<Post> findAll();
    List<Post> findAllByAuthor(String author);
}
