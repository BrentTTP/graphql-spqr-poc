package company.tothepoint.graphqlspqrpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlSpqrPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlSpqrPocApplication.class, args);
	}

}

