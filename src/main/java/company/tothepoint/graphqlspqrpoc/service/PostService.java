package company.tothepoint.graphqlspqrpoc.service;

import company.tothepoint.graphqlspqrpoc.model.Post;
import company.tothepoint.graphqlspqrpoc.repo.PostRepository;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotation.GraphQLApi;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@GraphQLApi
@Service
public class PostService {

    private PostRepository postRepository;

    @GraphQLQuery(name = "posts", description = "A list of all existing posts")
    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }

    @GraphQLQuery(name = "post", description = "One post fetched based on ID")
    public Post getPostById(@GraphQLArgument(name = "id") String id) {
        Optional<Post> oPost = postRepository.findById(new ObjectId(id));

        return oPost.orElse(null);
    }

    @GraphQLQuery(name = "postsOfAuthor", description = "A list of posts written by the given author")
    public List<Post> getPostsOfAuthor(@GraphQLArgument(name = "author") String author) {
        return postRepository.findAllByAuthor(author);
    }

    @GraphQLMutation(name = "createPost", description = "Add a new post")
    public Post addPost(@GraphQLArgument(name = "title") String title,
                        @GraphQLArgument(name = "content") String content,
                        @GraphQLArgument(name = "author") String author) {
        Post newPost = Post.builder().title(title).content(content).author(author).build();
        postRepository.save(newPost);
        return newPost;
    }
}
